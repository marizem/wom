
# About

1. Este directorio contiene los archivos:<br>
  * ``variables.tf`` => donde pueden ser cambiados los valores a ser usados en ``main.tf``.<br>
  * ``testing.tf`` => donde se pueden definir los valores usados en el ambiente de pruebas.<br>
2. El objetivo es craer un clústes de EKS en AWS.

# Requirements

=====================

NOTA: Este código se realizó usando Terraform 0.12.x syntax.

=====================

* Configuración de las credenciales de EKS e instalación de [kubectl](../../../tutorials/install_kubectl.md), [aws-cli](../../../tutorials/install_awscli.md), [terraform](../../../tutorials/install_terraform.md).

* Crear los siguientes recursos para el funcionamiento del clúster de EKS.
  * Bucket S3 y DynamoDB table por Terraform state remote;
  * Subnets public and private;
  * VPC;
  * NAT gateway;
  * Internet Gateway;
  * Security group;
  * Route table;
  * Policies.


## How to

* Cambie los valores según el ambiente necesitado en los archivos ``testing.tfvars`` y ``backend.tf`` .

* Valide la creación del ambiente con los siguientes comandos:

```bash
terraform init
terraform validate
terraform workspace new testing
terraform workspace list
terraform workspace select testing
terraform plan -var-file testing.tfvars
terraform apply -var-file testing.tfvars
terraform output
terraform show
```

Acceda al clúster con Kubectl.

```bash
aws eks --region REGION_NAME update-kubeconfig --name CLUSTER_NAME --profile PROFILE_NAME_AWS

kubectl get nodes -A

kubectl get pods -A
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.12.19, < 0.13.0 |
| aws | >= 3.22.0 |
| kubernetes | >= 1.11.2 |
| local | >= 1.4.0 |
| null | >= 2.1.2 |
| random | >= 2.2.1 |
| template | >= 2.1.2 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.22.0 |
| local | >= 1.4.0 |
| null | >= 2.1.2 |
| template | >= 2.1.2 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| address\_allowed | IP or Net address allowed for remote access. | `any` | n/a | yes |
| asg\_desired\_capacity | Number desired of nodes workers in cluster EKS. | `number` | n/a | yes |
| asg\_max\_size | Number maximal of nodes workers in cluster EKS. | `number` | n/a | yes |
| asg\_min\_size | Number minimal of nodes workers in cluster EKS. | `number` | n/a | yes |
| autoscaling\_enabled | Enable ou disable autoscaling. | `bool` | `true` | no |
| aws\_key\_name | Key pair RSA name. | `any` | n/a | yes |
| cluster\_enabled\_log\_types | A list of the desired control plane logging to enable.<br> For more information, see Amazon EKS Control Plane Logging documentation (https://docs.aws.amazon.com/eks/latest/userguide/control-plane-logs.html) | `list(string)` | <pre>[<br>  "api",<br>  "audit"<br>]</pre> | no |
| cluster\_endpoint\_private\_access | Indicates whether or not the Amazon EKS private API server endpoint is enabled. | `bool` | `true` | no |
| cluster\_endpoint\_private\_access\_cidrs | List of CIDR blocks which can access the Amazon EKS private API server endpoint, when public access is disabled | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| cluster\_endpoint\_public\_access | Indicates whether or not the Amazon EKS public API server endpoint is enabled. | `bool` | `true` | no |
| cluster\_endpoint\_public\_access\_cidrs | List of CIDR blocks which can access the Amazon EKS public API server endpoint. | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| cluster\_log\_retention\_in\_days | Number of days to retain log events. | `number` | `"7"` | no |
| cluster\_name | Cluster EKS name. | `any` | n/a | yes |
| cluster\_version | Kubernetes version supported by EKS. <br> Reference: https://docs.aws.amazon.com/eks/latest/userguide/kubernetes-versions.html | `any` | n/a | yes |
| credentials\_file | PATH to credentials file | `string` | `"~/.aws/credentials"` | no |
| cw\_retention\_in\_days | Fluentd retention in days. | `number` | `7` | no |
| environment | Name Terraform workspace. | `any` | n/a | yes |
| kubelet\_extra\_args | Extra arguments for EKS. | `string` | `"--node-labels=node.kubernetes.io/lifecycle=spot"` | no |
| lt\_name | Name of template worker group. | `any` | n/a | yes |
| map\_roles | Additional IAM roles to add to the aws-auth configmap.<br> See https://github.com/terraform-aws-modules/terraform-aws-eks/blob/master/examples/basic/variables.tf for example format. | <pre>list(object({<br>    rolearn  = string<br>    username = string<br>    groups   = list(string)<br>  }))</pre> | `[]` | no |
| map\_users | Additional IAM users to add to the aws-auth configmap.<br> See https://github.com/terraform-aws-modules/terraform-aws-eks/blob/master/examples/basic/variables.tf for example format. | <pre>list(object({<br>    userarn  = string<br>    username = string<br>    groups   = list(string)<br>  }))</pre> | `[]` | no |
| on\_demand\_percentage\_above\_base\_capacity | On demand percentage above base capacity. | `number` | n/a | yes |
| override\_instance\_types | Type instances for nodes workers. Reference: https://aws.amazon.com/ec2/pricing/on-demand/ | `list(string)` | n/a | yes |
| profile | Profile of AWS credential. | `any` | n/a | yes |
| public\_ip | Enable ou disable public IP in cluster EKS. | `bool` | `false` | no |
| region | AWS region. Reference: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-available-regions | `any` | n/a | yes |
| root\_volume\_size | Size of disk in nodes of cluster EKS. | `number` | n/a | yes |
| subnets | List of IDs subnets public and/or private. | `list(string)` | n/a | yes |
| suspended\_processes | Cluster EKS name. | `any` | n/a | yes |
| tags | Maps of tags. | `map` | `{}` | no |
| vpc\_id | ID of VPC. | `any` | n/a | yes |
| worker\_additional\_security\_group\_ids | A list of additional security group ids to attach to worker instances. | `list(string)` | `[]` | no |
| workers\_additional\_policies | Additional policies to be added to workers | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| cluster\_endpoint | Endpoint for EKS control plane. |
| cluster\_security\_group\_id | Security group ids attached to the cluster control plane. |
| config\_map\_aws\_auth | A kubernetes configuration to authenticate to this EKS cluster. |
| kubectl\_config | kubectl config as generated by the module. |
| region | AWS region. |
