
# About

1. Este directorio contiene:<br>
  * ``variables.tf`` => donde puedes definir las variable usadas en ``main.tf``.<br>
  * ``testing.tfvars`` => donde puedes definir los valores de las variable usadas en un ambiente de pruebas.<br>
2. El objetivo es crear toda la parte de networking requerida por el cluster de EKS en AWS.

# Requerimientos

=====================

NOTA: El desarrollo se hizo usando Terraform 0.12.x syntax.

=====================

* Configure las credenciales de AWS e instale [kubectl](../../../tutorials/install_kubectl.md), [aws-cli](../../../tutorials/install_awscli.md), [terraform](../../../tutorials/install_terraform.md).

* Creación de los siguientes recursos para el funcionamiento del cluster de EKS:
  * Bucket S3 y DynamoDB table por Terraform state remote;
  * Subnets public y private;
  * VPC;
  * NAT gateway;
  * Internet Gateway;
  * Security group;
  * Route table;
  * Policies.


## How to

* Cambie los valores según el ambiente de ejecución en los archivos ``testing.tfvars`` y ``backend.tf`` .

* Valide la creación de la estrcutura con los comandos siguientes:

```bash
terraform init
terraform validate
terraform workspace new testing
terraform workspace list
terraform workspace select testing
terraform plan -var-file testing.tfvars
terraform apply -var-file testing.tfvars
terraform output
terraform show
```

## Requerimientos

| Name | Version |
|------|---------|
| terraform | >= 0.12.19, < 0.13.0 |
| aws | >= 2.61.0 |
| local | >= 1.4.0 |
| null | >= 2.1.2 |
| random | >= 2.2.1 |
| template | >= 2.1.2 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 2.61.0 |
| local | >= 1.4.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| address\_allowed | IP or Net address allowed for remote access. | `any` | n/a | yes |
| aws\_key\_name | Key pair RSA name. | `any` | n/a | yes |
| aws\_public\_key\_path | PATH to public key in filesystem local. | `any` | n/a | yes |
| bucket\_name | Bucket name for storage Terraform tfstate remote. | `any` | n/a | yes |
| credentials\_file | PATH to credentials file | `string` | `"~/.aws/credentials"` | no |
| dynamodb\_table\_name | DynamoDB Table name for lock Terraform tfstate remote. | `any` | n/a | yes |
| environment | Name Terraform workspace. | `any` | n/a | yes |
| profile | Profile of AWS credential. | `any` | n/a | yes |
| region | AWS region. Reference: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-available-regions | `any` | n/a | yes |
| subnet\_private1\_cidr\_block | CIDR block to subnet private1. | `any` | n/a | yes |
| subnet\_public1\_cidr\_block | CIDR block to subnet public1. | `any` | n/a | yes |
| tags | Maps of tags. | `map` | `{}` | no |
| vpc1\_cidr\_block | CIDR block to vpc1. | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| bucket\_arn | Bucket ARN |
| bucket\_domain\_name | FQDN of bucket |
| bucket\_id | Bucket Name (aka ID) |
| key\_name | Name of key pair RSA |
| security\_group | Id of security Group |
| subnet\_private1 | Id of subnet private 1 |
| subnet\_public1 | Id of subnet public 1 |
| vpc1 | Id of VPC1 |
